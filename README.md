# Auto Kyuujitai (自動舊字體)

This font automatically converts any modern (post wwii) Japanese text into old Japanese (pre wwii) text.  
This means that all kanji (新字体) get converted into its traditional counterpart (舊字體) (like 体 → 體).

You can also set it to change modern kana usage (現代仮名遣い) to old kana usage (歷史的假名遣ひ) (like かえる → かへる) (to the best of TTFs ability).

## Installation

First download [sukima](https://gitlab.com/ekaunt/auto-kyuujitai/-/raw/master/sukima_auto-kyuujitai.ttf) for a sans font, and/or [hanamin](https://gitlab.com/ekaunt/auto-kyuujitai/-/raw/master/hanamin_auto-kyuujitai.ttf) for a serif font. Then follow the instructions below to set it to your default font of your OS.

 - Windows:
   - Download [Meiryo UIも大っきらい](https://www.vector.co.jp/soft/winnt/util/se500183.html) and open it and set the font there, then reboot and you'll be golden.
 - Linux:
   - Download [Fontweak](https://github.com/guoyunhe/fontweak/) and set the font there, or set the font manually through fontconfig.

## Features

### Kyuujitai

This is a tricky thing to get because while the traditional character of 知 remains 知 and the traditional character of 恵 is 惠, the word 知恵 used to be written as 智慧, so I had to account for that.

### More Kanji

Japanese used to be written with a lot more kanji. So I try as best as I can to insert kanji in places where there used to be kanji but isn't anymore (like この → 此の).
I also added a lot of automatic ateji (當て字) replacements and english kanji substitutions like ページ → 頁.

### Other Features

I added some more features like kana repeating marks (そそぐ → そゝぐ).

Turn 々 into 〻 in vertical text.

## Shortcomings

This font was supposed to be fully featured and also convert words like 変える to 變へる, but due to shortcomings in modern font formats its currently not possible to do so. See [this issue](https://github.com/fontforge/fontforge/issues/4545#issuecomment-750741001)
