#!/usr/bin/env python

features = ""
features += ("languagesystem DFLT dflt;\nlanguagesystem kana dflt;\nlanguagesystem latn dflt;\n\n")

def join(arr):
    # print(arr)
    return " ".join(arr)

lookup_num = 0
def replace(orig, dest, back=None, ahead=None):
    global lookup_num
    lookup_num += 1
    # convert everything to its unicode codepoint
    orig = us(orig)
    dest = us(dest)
    if back:
        # print(back)
        back = us(back)
    if ahead:
        ahead = us(ahead)

    # set n to the difference in size
    n=0
    # larger=""
    origL=destL=[]
    if len(orig) > len(dest):
        n=len(orig)-len(dest)
        origL = orig[-n:]
        # del orig[-n:]
        # larger="orig"
    elif len(orig) < len(dest):
        n=len(dest)-len(orig)
        origL = orig[-n:]
        # del orig[-n:]
        # larger="dest"
    # print(orig, origL, dest, destL)

    global features
    features += "\nlookup sub"+str(lookup_num)+" {\n"
    for lookup in ["\tsub "+orig[i]+" by "+dest[i]+" ;\n" for i in range(len(orig)-n)]:
        features += lookup
    for lookup in ["\tsub "+origL[i]+" by emptyGlyph ;\n" for i in range(len(origL))]:
        features += lookup
    for lookup in ["\tsub "+destL[i]+" by emptyGlyph ;\n" for i in range(len(destL))]:
        features += lookup
    features += ("} sub"+str(lookup_num)+";\n\n")

    features += ("feature calt {\n")
    for script in ["DFLT","cyrl","geor","grek","hebr","kana","latn","hani"]:
        features += ("\n\tscript "+script+";\n\t\tlanguage dflt ;\n")

        # go through each character, and add a substitution for it.
        for i in range(len(orig)):
            features += ("\t\tsub ")
            if back:
                features += (" [ "+join(back)+" ] ")

            if len(orig) == 1:
                features += orig[i]+"' lookup sub"+str(lookup_num)
            elif i == 0:
                features += (orig[i]+"' lookup sub"+str(lookup_num)+" "+ join(orig[i+1:]))
            elif i == len(orig)-1:
                if back:
                    features += (" [ "+join(back)+" ] ")
                features += (join(dest[:i])+" "+orig[i]+"' lookup sub"+str(lookup_num))
            else:
                if back:
                    features += (" [ "+join(back)+" ] ")
                features += (join(dest[:i])+" "+orig[i]+"' lookup sub"+str(lookup_num)+" "+join(orig[i+1:]))

            if ahead:
                features += (" [ "+join(ahead)+" ] ")
            features += (" ;\n")
        
        # if orig is longer than dest, then we'll complete the 
        # rest of the substitution with a ligature substitution
        # if dest is longer than orig, then we'll complete the
        # rest of the substitution with a multiple substitution
        # features += "lookup clig"+str(lookup_num)+" {\n\tsub "
        
        # if len(orig) > len(dest):
            # features += "lookup clig"+str(lookup_num)+" {\n\tsub "
            # print(eval("len("+larger+")-n"))
            # for dif in range(n):
                # if back:
                    # features += (" [ "+join(back)+" ] ")
                # features += orig[-n+dif] + "' "
                # if ahead:
                    # features += (" [ "+join(ahead)+" ] ")
                # features += " by "
            # features += "\n} clig"+str(lookup_num)+" ;"


    features += ("} calt;\n")

def s(orig, dest):
    replace(orig, dest)
def _s(orig, dest, back):
    replace(orig, dest, back=back)
def s_(orig, dest, ahead):
    replace(orig, dest, ahead=ahead)
def _s_(orig, dest, back, ahead):
    replace(orig, dest, back=back, ahead=ahead)


cps = {}
def u(char):
    # get codepoint of char
    if not char in cps.keys():
        cps[char] = str('uni%04X' % ord(char))
    return cps[char]
def us(chars):
    result = []
    for char in chars:
        result.append(u(char))
    return result

# s("abc", "ghi")
# _s("abc", "ghi", "きt")
# s_("xy", "pq", "bj")
# _s("きよう", "きやう", "無な") #無きよう
# _s_("abc", "ghi", back="eight t", ahead="x one j")
s("でたらめ", "出鱈目")

print(features)
# print(us("無きよう"))
